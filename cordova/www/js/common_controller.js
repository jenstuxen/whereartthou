/**
 * Common Functionality for app (cordova and webbrowser)
 */

var host = 'where.artthou.net';
var restPrefix = '/rest/v1/';
var restBaseUrl = 'https://' + host + restPrefix;
var ws = 'wss://' + host + '/ws';
var userId = makeid(5);
var sessionId = makeid(12);
var socket;
var map;
var gps;
var zoom = 17;
var targets = Object();
var targetsLayer;
var historyLayer;
var history;
var restTargets;


function latest() {
    $.getJSON( restBaseUrl+"room/"+sessionId+"/latest", function( data ) {
      var items = [];
       $.each( data.targets, function( target ) {
            if (this.locs.length > 0) { //gots to have locs
                this.loc = this.locs[0]; //just convert to event style
                updateTarget(this);
            }
       });
       //setTargets();
     });
}

function all() {
    $.getJSON( restBaseUrl+"room/"+sessionId, function( data ) {
      drawHistory(data);
     });

}


function initMap() {
    map = L.map('map_canvas');
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}   ).addTo(map);
    historyLayer = L.layerGroup();
    targetsLayer = L.layerGroup();
    historyLayer.addTo(map);
    targetsLayer.addTo(map);
    history = setInterval(all, 120000);
    restTargets = setInterval(latest, 15000);

}



function setRoomId() {
    $('#currentRoom').html(sessionId);
    $('#roomJoiner').val(sessionId);
}

function bindJoinLeaveButtons() {
    $("#leave").on("tap", function() {
            var leave = new Object();
            leave.leave = new Object();
            leave.leave.sessionId = sessionId;
            leave.leave.userId = userId;
            console.log(JSON.stringify(leave));
            socket.send(JSON.stringify(leave));
            sessionId = makeid(12);
            setRoomId();
        }
    );
    
    $("#join").on("tap", joinRoom);
}

function joinRoom() {
    var s = $("#roomJoiner").val();
    var join = new Object();
    join.join = new Object();
    join.join.sessionId = s;
    join.join.userId = userId;
    console.log(JSON.stringify(join));
    sessionId = s;
    setRoomId();
    socket.send(JSON.stringify(join));
}

function fixContentHeight() {
    var screen = $.mobile.getScreenHeight();
    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight()  - 1 : $(".ui-header").outerHeight();
    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
    /* content div has padding of 1em = 16px (32px top+bottom). This step
       can be skipped by subtracting 32px from content var directly. */
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer - contentCurrent;
    $(".ui-content").height(content);
    $("#map_canvas").height(content);
}

function onNewPos(position) {
    
    var positionEvent = new Object();
    positionEvent.sessionId = sessionId;
    positionEvent.userId = userId;
    positionEvent.loc = new Object();
    positionEvent.loc.lat = position.coords.latitude;
    positionEvent.loc.lon = position.coords.longitude;
    positionEvent.loc.timestamp = Math.floor(position.timestamp/1000);
    positionEvent.loc.accuracy = position.coords.accuracy;
    
    if (position.coords.speed !== null) {
        positionEvent.loc.speed = position.coords.speed;
    }
    if (position.coords.heading !== null) {
        positionEvent.loc.heading = position.coords.heading;
    }
    if (position.coords.altitudeAccuracy !== null) {
        positionEvent.loc.altitudeAccuracy = position.coords.altitudeAccuracy;
    }

    try {
        socket.send(JSON.stringify(positionEvent));
    } catch (exception) {
        console.log(exception.message);
    }

    
}


function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
}

function updateTarget(obj) {
    if (!obj['loc'] || !obj['loc']['lat'] || !obj['loc']['lon']) {
        return;
    }
    if (!obj['loc']['speed']) {
        obj['loc']['speed'] = 0;
    }
    if (!obj['loc']['accuracy']) {
        obj['loc']['accuracy'] = 50;
        color = 'red';
    } else {
        color = 'green';
    }
    if (!obj['loc']['heading']) {
        obj['loc']['heading'] = 0;
    }
    if (!obj['loc']['altitude']) {
        obj['loc']['altidude'] = 0;
    }
    targets[obj['userId']] = obj;    
    setTargets();
}


function setTargets() {
    
    map.removeLayer(targetsLayer);
    var circles = [];
    var bounds = [];
    var colors = ['red','green','blue','yellow','pink','orange'];
    var count = 0;

    h = '<ol data-role="listview" data-inset="true">';
    $.each(targets, function (key, value) {
        h += '<li"><a class="target" href="#">'+value['userId']+'</a></li> ';
        bounds.push([value['loc']['lat'],value['loc']['lon']]);
        circles.push(L.circle([value['loc']['lat'], 
            value['loc']['lon']], 
            value['loc']['accuracy'],
            {color: colors[count], fillColor: colors[count], fillOpacity: 0.2}).bindPopup(value['userId']));
            
        var marker = L.marker([value['loc']['lat'], value['loc']['lon']]);
        circles.push(marker);
        count = (count + 1) % 5;
    });
    h += '</ol>';
    
    $('#targets_content').html(h);
    $('#map_footer').html(h);

    targetsLayer = L.layerGroup(circles);
    targetsLayer.addTo(map);
    
    map.fitBounds(bounds);
    
}

function drawHistory(data) {
    var r
    try {
        r = $.parseJSON(data);
    } catch(err) {
        r = data;
    }
    historyLayer.clearLayers();

    $.each(r.targets, function(i, target) {
        var latlng = new Array();
        $.each(target.locs, function(x, location) {
            latlng[x] = new L.LatLng(location.lat, location.lon);
        });

        new L.Polyline(latlng, {
            color: 'red',
            weight: 3,
            opacity: 0.5,
            smoothFactor: 1
        }).addTo(historyLayer);
    }) ;
}

function makeid(x) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < x; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function initWS() {
    websocket = new WebSocket(ws);
    websocket.onopen = function (evt) {
        console.log("CONNECTED");
        joinRoom();
    };
    websocket.onclose = function (evt) {
        console.log("CLOSED");
        setTimeout('initWS()', 30000);
    };
    websocket.onmessage = function (evt) {
        j = $.parseJSON(evt.data);
        if (j['nop'] != null) {
            console.log(evt.data);
        } else if (j['loc'] != null) {
            console.log("updating target "+ JSON.stringify(j));
            updateTarget(j);
        } else if (j['leave'] != null) {
            console.log("target "+j['leave']['userId']+" is leaving");
        } else if (j['join'] != null) {
            console.log("target "+j['join']['userId']+" joined");
        }
        
        else {
            console.log("Received unknown op: "+evt);
        }
    };
    websocket.onerror = function (evt) {
        console.warn("ERROR: ");
        console.warn(evt);
        console.warn(evt.data);
    };
    
    socket = websocket;
}
