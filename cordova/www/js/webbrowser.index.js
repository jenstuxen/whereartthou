var app = {    
    onDeviceReady: function () {
        setRoomId();
        bindJoinLeaveButtons();
        bindGPSOnOff()
        fixContentHeight();
        
        initMap();


        initWS();
    }
};

$( document ).ready(function() {
	sessionId = $("#currentRoom").html();
	app.onDeviceReady();
});

function bindGPSOnOff() {
    $("#shareLocation").on("change", function() {
        //alert('changed');
        if (this.checked) {
            gps = navigator.geolocation.watchPosition(onNewPos, error, {timeout: 3000000, enableHighAccuracy: true});
            
        } else {
            navigator.geolocation.clearWatch(gps);
        }



    });
}
