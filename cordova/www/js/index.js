var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
        
    },
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("resume", this.onResume, false);
        document.addEventListener("pause", this.onPause, false);
        document.addEventListener("offline", this.onOffline, false);
        document.addEventListener("online", this.onOnLine, false);
    },
    
    onResume: function () {
        navigator.geolocation.clearWatch(gps);

        gps = navigator.geolocation.watchPosition(onNewPos, error, {timeout: 300000, enableHighAccuracy: true});
    },
    
    onPause: function () {
        navigator.geolocation.clearWatch(gps);
        gps = navigator.geolocation.watchPosition(onNewPos, error, {timeout: 300000, enableHighAccuracy: false});
    },
    
    onOffline: function() {
        navigator.geolocation.clearWatch(gps);
    },
    
    onOnline: function() {
        navigator.geolocation.clearWatch(gps);
        gps = navigator.geolocation.watchPosition(onNewPos, error, {timeout: 300000, enableHighAccuracy: true});
    },
    
    onDeviceReady: function () {
        
        setRoomId();
        bindSendMessage();
        bindPickContact();
        bindJoinLeaveButtons();
        fixContentHeight();
        
        initMap();

        //ios fix
        if (typeof StatusBar !== 'undefined') {
            StatusBar.hide();
        }
        
        initWS();
        $(".target").on("tap",function(){
        console.log('tapped on '+$(this).text());
        t = targets[$(this).text()];
        map.setView([t['loc']['lat'], t['loc']['lon']], 14);
        
    });
    }
};


}


app.initialize();