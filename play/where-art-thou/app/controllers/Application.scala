package controllers

import java.util.concurrent.ConcurrentHashMap
import java.util.function.BiFunction

import com.google.common.cache.{CacheBuilder, CacheLoader, LoadingCache}
import models._
import play.api.Play.current
import play.Play
import play.api.cache.Cache
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee._
import play.api.libs.json.Json.toJsFieldJsValueWrapper
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.libs.ws.WS
import play.api.mvc.{Action, Controller, WebSocket}

import scala.collection.JavaConversions.{collectionAsScalaIterable, mapAsScalaConcurrentMap}
import scala.concurrent.Future


object App extends Controller {

  def index = Action {
    val endpoints = List(
      "GET /rest/v1/room/<roomId>/user/<userid>/pos/<lat>/<lon>/<timestamp>" + " : " + "set position in room id, for user ",
      "GET /rest/v1/room/<roomid>" + " : " + "get positions for all victims in room roomid",
      "GET /rest/v1/room/latest" + " : " + "get the latest position for all users in room",
      "WEBSOCKET /ws : real-time events")

    Ok(views.html.index(endpoints))
  }

  def ping = Action {
    Ok("pong")
  }

  def view(sessionId: String) = Action {
    Ok(views.html.v(sessionId))
  }

  def viewBasic() = Action {
    Ok(views.html.v("100"))
  }

  private def updatePos(sessionId: String, userId: String, lat: Double, lon: Double, timestamp: Long): Unit = {
    val room: Room = cache().get(sessionId)
    room.targets.putIfAbsent(userId, Target(userId, List[Location]()))

    val bifun = new BiFunction[String,Target,Target] {
      override def apply(t: String, u: Target): Target = u.add(new Location(lat,lon,timestamp))
    }

    room.targets.compute(userId,bifun)
  }

  private def updatePosEvent(sessionId: String, event: JsValue): Unit = {
    val room: Room = cache().get(sessionId)
    room.channels.values.foreach { channel => channel push event }
  }

  def pos(sessionId: String, userId: String, lat: Double, lon: Double, timestamp: Long) = WithCors("GET", "POST") {
    Action {
      updatePos(sessionId, userId, lat, lon, timestamp)

      val event = Json.toJson(new Event(sessionId, userId, Some(new Location(lat, lon, timestamp))))
      updatePosEvent(sessionId, event)

      Ok(event)
    }
  }

  def room(sessionId: String) = WithCors("GET", "POST") {
    Action {
      val room: Room = cache.get(sessionId)
      Ok(Json.toJson(room))
    }
  }

  def roomLatest(sessionId: String) = WithCors("GET", "POST") {
    Action {

      val room = cache.get(sessionId);

      val latests = new ConcurrentHashMap[String, Target]();
      room.targets.values.foreach { t => latests.put(t.userId, new Target(t.userId, List(t.locs.headOption).flatten)) }

      Ok(Json.toJson(new Room(sessionId, latests)))
    }
  }

  private def cache(): LoadingCache[String, Room] = {
    var c = Cache.getAs[com.google.common.cache.LoadingCache[String, Room]]("stateoftheunion").orNull

    if (c == null) {
      c = CacheBuilder.newBuilder().expireAfterAccess(4, java.util.concurrent.TimeUnit.HOURS).build(new CacheLoader[String, Room] {
        def load(sessionId: String): Room = {
          val (out, channel) = Concurrent.broadcast[JsValue]
          return Room(sessionId)
        }
      })

      Cache.set("stateoftheunion", c)
    }

    return c
  }

  private def FCMMulticast(evt: Event, to: String) = Future {
    val url = "https://fcm.googleapis.com/fcm/send";
    val f = WS.url(url).withHeaders("Content-Type" -> "application/json", "Authorization" -> current.configuration.getString("fcm.key").get)
    val reg_ids = cache().get(evt.sessionId).subscribed.get.toArray;

    f.post(GCMBodyWrites.writes(GCMBody(evt,to,reg_ids)));
  }

  def subscribe(sessionId: String, clientToken: String) = Action {
    val c = cache().get(sessionId)
    c.subscribe(clientToken)
    Ok("subscribed")
  }

  def ws = WebSocket.using[JsValue] { request => {
    val (out, ch) = Concurrent.broadcast[JsValue];
    val enum = Concurrent.unicast((ch: Concurrent.Channel[JsValue]) => {}, Unit, (msg: String, input: Input[JsValue]) => {
      play.Logger.info("Websocket Enumerator Encountered error: " + msg)
    })

    val in = Iteratee.foreach[JsValue](e => {
      play.Logger.info("raw event: " + e)


      //location event
      e.asOpt[Event] match {
        case Some(event) => {
          play.Logger.info(event.toString())
          val room = cache().get(event.sessionId)
          room.targets.putIfAbsent(event.userId, Target(event.userId, List[Location]()))
          room.targets.get(event.userId).add(event.loc.get)
          room.channels.update(event.userId, ch)
          room.channels.values.foreach { channel => channel push e }
        }
        case _ => {}

      }

      //join event
      e.asOpt[JoinEvent] match {
        case Some(j) => {
          play.Logger.info(j.toString())
          val room = cache().get(j.join.sessionId)
          room.targets.putIfAbsent(j.join.userId, Target(j.join.userId, List[Location]()))
          room.channels.update(j.join.userId, ch)
          room.channels.values.foreach { channel => channel push e }
        }
        case _ => {}
      }

      e.asOpt[LeaveEvent] match {
        case Some(l) => {
          play.Logger.info(l.toString())
          val room = cache().get(l.leave.sessionId)
          room.targets.remove(l.leave.userId)
          room.channels.remove(l.leave.userId)
          room.channels.values.foreach { channel => channel push e }
        }
        case _ => {}
      }
    })

    (in, out.andThen(enum))

  }
  }


  implicit val LocationReads = Json.reads[Location]
  implicit val LocationWrites = Json.writes[Location]
  implicit val positionEventReads = Json.reads[Event]
  implicit val positionEventWrites = Json.writes[Event]

  implicit val NopWrites = Json.writes[Nop]
  implicit val JoinEventWrites = Json.writes[JoinEvent]
  implicit val LeaveEventWrites = Json.writes[LeaveEvent]

  implicit val JoinEventReads = Json.reads[JoinEvent]
  implicit val LeaveEventReads = Json.reads[LeaveEvent]


  implicit val targetReads = Json.reads[Target]
  implicit val targetWrites = Json.writes[Target]



  implicit val GCMBodyWrites = Json.writes[GCMBody]



  implicit val roomWrites = new Writes[Room] {
    def writes(room: Room): JsValue = {
      Json.obj(
        "sessionId" -> room.sessionId,
        "targets" -> room.targets.values().toList)
    }
  }


}