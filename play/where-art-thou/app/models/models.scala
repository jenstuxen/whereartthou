package models

import java.util.concurrent.atomic.AtomicReference
import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}

import play.api.http.HeaderNames._
import play.api.libs.iteratee.Concurrent
import play.api.libs.json.JsValue
import play.api.mvc._

import scala.concurrent._

case class Location(lat: Double,lon: Double,timestamp: Long, speed: Option[Long] = None, altitude: Option[Long] = None, accuracy: Option[Long] = None, altitudeAccuracy: Option[Long] = None, heading: Option[Long] = None)
case class Target(userId: String, locs: List[Location], handle: Option[String] = None) {
  def add(loc: Location): Target = {
    this.synchronized {
      
      //every 200 positions, save half of them
      if (locs.size > 200) {
        Target(userId, loc::skip(locs,2),handle)
      }
      
      Target(userId, loc::locs, handle)
    }
  }
  
  private def skip[A](l:List[A], n:Int) = 
    l.zipWithIndex.collect {case (e,i) if ((i+1) % n) == 0 => e} // (i+1) because zipWithIndex is 0-based
  
}
case class Event(sessionId: String, userId: String, loc: Option[Location] = None)
case class JoinEvent(join: Event)
case class LeaveEvent(leave: Event)

case class Nop(nop: String)
case class Room(sessionId: String,
                targets: ConcurrentMap[String, Target] = new ConcurrentHashMap(),
                channels: ConcurrentMap[String, Concurrent.Channel[JsValue]] = new ConcurrentHashMap(),
                subscribed: AtomicReference[Set[String]] = new AtomicReference[Set[String]](Set())) {

  //non blocking update
  def subscribe(token: String): Unit = {
    while (true) {
      val s = subscribed.get()
      val n = s.+(token)
      if (subscribed.compareAndSet(s, n)) return
    }
  }
}

case class GCMBody(data: Event, to: String, registration_ids: Array[String])

/**
 * Action decorator that provide CORS support
 *
 * @author Giovanni Costagliola, Nick McCready
 */
case class WithCors(httpVerbs: String*)(action: EssentialAction) extends EssentialAction with Results {
    def apply(request: RequestHeader) = {
        implicit val executionContext: ExecutionContext = play.api.libs.concurrent.Execution.defaultContext
        val origin = request.headers.get(ORIGIN).getOrElse("*")
        if (request.method == "OPTIONS") { // preflight
            val corsAction = Action {
                request =>
                    Ok("").withHeaders(
                        ACCESS_CONTROL_ALLOW_ORIGIN -> origin,
                        ACCESS_CONTROL_ALLOW_METHODS -> (httpVerbs.toSet + "OPTIONS").mkString(", "),
                        ACCESS_CONTROL_MAX_AGE -> "3600",
                        ACCESS_CONTROL_ALLOW_HEADERS ->  s"$ORIGIN, X-Requested-With, $CONTENT_TYPE, $ACCEPT, $AUTHORIZATION, X-Auth-Token",
                        ACCESS_CONTROL_ALLOW_CREDENTIALS -> "true")
            }
            corsAction(request)
        } else { // actual request
            action(request).map(res => res.withHeaders(
                ACCESS_CONTROL_ALLOW_ORIGIN -> origin,
                ACCESS_CONTROL_ALLOW_CREDENTIALS -> "true"
            ))
        }
    }
}
