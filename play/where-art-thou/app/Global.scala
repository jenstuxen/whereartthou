import com.google.common.cache.LoadingCache
import models._
import play.api.{Application, GlobalSettings}
import play.api.Play.current
import play.api.cache.Cache
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json

import scala.collection.JavaConversions.collectionAsScalaIterable
import scala.concurrent.duration.DurationInt

object Global extends GlobalSettings {
  implicit val NopWrites = Json.writes[Nop]


  override def onStart(app: Application) {
    val nop = Nop("")
    Akka.system(app).scheduler.schedule(30 seconds, 30 seconds, new Runnable {
      def run() {
         Cache.getAs[LoadingCache[String, Room]]("stateoftheunion") match {
          case Some(c) => {
            c.asMap().values().foreach { room => room.channels.values().foreach { channel => channel push Json.toJson(nop) } }
          }
          
          case _ => None
        }
        
        

      }
    })
  }

}



