# Project: Where Art Thou
### A real-time websocket/google cloud messaging, gps location sharing application, without usernames or logins. ##

##Tech:
1. Play for scalable rest and websocket api (Scala)
2. Docker provisioning
3. Cordova for Android/IOS (Javascript)
4. A native Android service (Java)
5. A no-dependencies browser client for Mobile and Desktop without app installation (Javascript)